const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = Schema({


	firstName:{
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email  is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false,	
	},
	isSuperAdmin:{

		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},

	cartItems:[
		{
			_id:{
			type: Schema.Types.ObjectId, ref: 'Product'
			},
			quantity:{
			type: Number,
			default: 1
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);


