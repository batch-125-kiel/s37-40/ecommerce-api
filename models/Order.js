const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = Schema({

	buyerId:{
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	totalAmount: {
		type: Number,
		required: [true, "description is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	items:[{
		_id: false,
		productId:{
			type: Schema.Types.ObjectId, ref: 'Product'
		},
		quantity:{
			type: Number,
			default: 1
		}
	}]
	
})

module.exports = mongoose.model("Order", orderSchema);