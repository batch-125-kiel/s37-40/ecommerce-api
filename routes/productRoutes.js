const express = require('express');
const router = express.Router();
const productController = require('./../controllers/productControllers');
const auth = require('./../auth');
const fileUpload = require('express-fileupload');
const fs = require("fs");


router.use(fileUpload());

router.post('/create',auth.verify, (req, res)=>{

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.createProduct(req.body,isAdmin).then(result => res.send(result));

})

//show all active, for regular users
router.get('/',(req, res)=>{

	productController.showAllActive().then(result => res.send(result));

})
//show all for admin including inactive

router.get('/adminProductList',auth.verify, (req, res)=>{

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.showAdminProductList(isAdmin).then(result => res.send(result));

})



router.get('/id/:id', (req, res)=>{

	let id = req.params.id;
	productController.showSingle(id).then(result => res.send(result));

})

router.put('/:id',auth.verify, (req, res)=>{

	let id = req.params.id;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(id, req.body, isAdmin).then(result => res.send(result));

})

router.put('/:id/archive', auth.verify, (req, res)=>{

	let id = req.params.id;
	let productIsActive = req.body.isActive;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.archiveProduct(id,isAdmin,productIsActive).then(result=> res.send(result));

})
router.put('/:id/unarchive', auth.verify, (req, res)=>{

	let id = req.params.id;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.unarchiveProduct(id,isAdmin).then(result=> res.send(result));

})

router.delete('/:id/delete', auth.verify, (req, res)=>{

	let id = req.params.id;
	let productIsActive = req.body.isActive;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.deleteProduct(id,isAdmin).then(result=> res.send(result));

})


router.post('/upload/:image/:imageOld', (req, res)=>{

	let newImageName = req.params.image;
	let OldImageName = req.params.imageOld;

	console.log(`OLD IMAGE TO!!!!`,OldImageName);

	if(OldImageName !== null && OldImageName!== newImageName){

		const pathToFile = "./public/images/" + OldImageName;

		fs.unlink(pathToFile, function(err) {
		  if (err) {
		    console.log(err)
		  } else {
		    console.log("Successfully deleted the file.")
		  }
		})

	}



	if(!req.files){

		console.log('No file uploaded');
		

	}else{


		let sampleFile = req.files.File;

		let uploadPath= './public/images/'+ newImageName;

	
		sampleFile.mv(uploadPath, function(err) {

		   if (err)

		     return res.status(500).send(err);

		   res.send('File uploaded!');
		 });


	}


})

router.get('/featuredProducts', (req, res)=>{


	productController.showFeaturedProducts().then(result=> res.send(result));


})




module.exports = router;