const express = require('express');
const router = express.Router();
const userController = require('./../controllers/userControllers');
const orderController = require('./../controllers/orderControllers');//created separate controller for order
const auth = require('./../auth');
const nodemailer = require('nodemailer');



//console.log("HELLO");

router.post('/register', (req, res)=>{

	userController.register(req.body).then(result => res.send(result));

})
router.post('/login', (req, res)=>{

	userController.login(req.body).then(result => res.send(result));
})

router.get('/details', auth.verify, (req, res)=>{

	const userId = auth.decode(req.headers.authorization).id;
	userController.userDetails(userId).then(result => res.send(result));
})

router.put('/:userId/setAsAdmin',auth.verify, (req, res)=>{


	console.log('PUMASOK')

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let userId = req.params.userId;
	let userIsAdmin = req.body.userIsAdmin;

	userController.setAsAdmin(userId, isAdmin, userIsAdmin).then(result => res.send(result));

})

router.post('/checkout', auth.verify, (req, res)=>{


	const buyerId = auth.decode(req.headers.authorization).id;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;


	orderController.checkoutOrder(req.body,buyerId, isAdmin).then(result => res.send(result));

})

router.get('/orders', auth.verify, (req, res) =>{

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	orderController.showOrders(isAdmin).then(result=> res.send(result));

})

router.get('/myOrders', auth.verify, (req, res)=>{


	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id;

	orderController.showMyOrders(userId,isAdmin).then(result=> res.send(result));

})

router.get('/list', auth.verify, (req, res)=>{

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	userController.showAllUsers(isAdmin).then(result => res.send(result));

})

router.put('/addToCart/:productId', auth.verify, (req, res)=>{

	const userId = auth.decode(req.headers.authorization).id;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let productId = req.params.productId;
	let qtyValue = req.body.quantity;


	console.log(qtyValue);

	userController.addToCart(userId, isAdmin, productId, qtyValue).then(result => res.send(result));

})

router.get('/getCartItems', auth.verify, (req, res)=>{

	const userId = auth.decode(req.headers.authorization).id;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	userController.getCartItems(userId, isAdmin).then(result => res.send(result));

})

router.put('/removeFromCart/:id', auth.verify, (req, res)=>{

	const productId = req.params.id;
	const userId = auth.decode(req.headers.authorization).id;
	userController.removeFromCart(userId,productId).then(result=> res.send(result));

})

router.put('/clearCart', auth.verify, (req, res)=>{

	const userId = auth.decode(req.headers.authorization).id;
	userController.clearCart(userId).then(result=> res.send(result));

})

router.put('/changeQtyCart', auth.verify, (req, res)=>{

	const userId = auth.decode(req.headers.authorization).id;
	const operation =  req.body.operation;
	const pId =  req.body.pId;

	userController.changeQtyCart(userId, operation, pId).then(result=> res.send(result));

})

router.post('/contact', (req, res)=>{


	const transporter = nodemailer.createTransport({

		service: "gmail",
		auth: {

			user: "paoro.tobimasu@gmail.com",
			pass: "damienzk.exe"
		}

	})


	console.log(req.body.formData);

	const mailOptions = {

		from: req.body.formData.email,
		to: "paoro.tobimasu@gmail.com",
		subject: `Message from ${req.body.formData.name}, ${req.body.formData.email}, ${req.body.formData.contactNo}`,
		text: req.body.formData.message

	}

	transporter.sendMail(mailOptions, (error, info)=>{


		if(error){
			console.log('MAY ERROR');
			res.send(error)

		}else{

			console.log('WALA ERROR');

			res.send("Success");
		}


	})

	
})





module.exports = router;