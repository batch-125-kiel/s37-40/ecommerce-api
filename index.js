require('dotenv').config();
const environment = process.env.NODE_ENV || 'development'; // You can set NODE_ENV as an environment variable.
const MONGODB_URI = process.env.MONGODB_URI;

const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 4000;
const app = express();
const cors = require('cors');

const path = require('path');
app.use('/static', express.static(path.join(__dirname, 'public')));

//const bodyParser = require('body-parser');
//removed body parser

let userRoutes = require('./routes/userRoutes');
let productRoutes = require('./routes/productRoutes');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
//app.use(bodyParser.urlencoded({extended: true}));
//removed body parser

mongoose
  .connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connected to the database.'))
  .catch((error) => console.log(error));

//ROUTES
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);

app.listen(PORT, () => console.log(`Running in port ${PORT}, ${environment} environment.`));

//SCHEMA
