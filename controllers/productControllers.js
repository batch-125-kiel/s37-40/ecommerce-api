const Product = require('./../models/Product');
const auth = require('./../auth');
const fs = require("fs");


module.exports.createProduct = (reqBody, isAdmin=true)=>{

	if(isAdmin === true){


		let newProduct = new Product({

			name: reqBody.name,
			description: reqBody.description,
			price:reqBody.price,
			image:reqBody.image,
			isFeatured:reqBody.isFeatured

		})

			return newProduct.save().then((result, error)=>{

				console.log(result);

				return error ? false : result;
		})

	}else{

		return Promise.resolve("Not authorized to access this page");
	}

}

module.exports.showAllActive = ()=>{

	return Product.find({isActive: true}).sort({createdOn: -1}).then((result, error)=>{

		return error ? error : result;
	})
}

//shows all wether inactive or active
module.exports.showAdminProductList = (isAdmin)=>{

	if(isAdmin === true){

		return Product.find().sort({createdOn: -1}).then((result, error)=>{

			return error ? error : result;
		})

	}else{

		return Promise.resolve("Not authorized to access this page");

	}


}


module.exports.showSingle = (id)=>{

	//error doesnt throw anything, fix that
	return Product.findOne({_id: id}).then((result, error)=>{

		if(result != undefined || result != null){


			return result;

		}else{

			return false;

		}


	})
}

module.exports.updateProduct = (id,reqBody,isAdmin)=>{

	if(isAdmin === true){


		return Product.findByIdAndUpdate(id,reqBody).then((result, error)=>{


			

			return error ? error : result;

		})

	}else{

		return Promise.resolve('Not authorized to access this page');
	}

}

module.exports.archiveProduct =(id, isAdmin, productIsActive)=>{


	if(isAdmin === true){

		let amendStatus;
		let message;
																	//since cannot get body

		if (productIsActive === true || productIsActive == undefined) {//set as undefined for now, so that it works in postman

			amendStatus = false;
			message = true;

		}else{

			amendStatus = true;
			message = false;
		}

		return Product.findByIdAndUpdate(id,{isActive: amendStatus}, {new: true}).then((result, error)=>{

			return error ? error : {archived: message};

		})

	}else{

		return Promise.resolve('Not authorized to access this page');
	}

}

module.exports.unarchiveProduct =(id, isAdmin)=>{
	
	if(isAdmin === true){

		return Product.findByIdAndUpdate(id,{isActive: true}).then((result, error)=>{

			return error ? error : {unarchived: true};

		})

	}else{

		return Promise.resolve('Not authorized to access this page');
	}

}



module.exports.deleteProduct = (id, isAdmin)=>{

	if (isAdmin === true) {

		return Product.findByIdAndDelete(id).then((result, error)=>{


			if(result){

				const pathToFile = "./public/images/" + result.image;

				fs.unlink(pathToFile, function(err) {
				  if (err) {
				    console.log(err);
				  } else {
				    console.log("Successfully deleted the file.")
				  }
				})

			}

			return error ? error : {deleted: true};

		})
	}else{

		return Promise.resolve('Not authorized to access this page');
	}


}

module.exports.showFeaturedProducts = ()=>{

	return Product.find({isFeatured:true, isActive: true}).sort({createdOn: -1}).then((result)=>{

		console.log(result);
		return result;

	})
	.catch(error=>{

		console.log(error);
		return error;


	})



}