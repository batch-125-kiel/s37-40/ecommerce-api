const User = require('./../models/User');
const bcrypt = require('bcrypt');
const auth = require('./../auth');


module.exports.register = (reqBody)=>{

	//check if email already exists before registering new user
	return User.find({email: reqBody.email}).then((result, error) =>{

			if(result.length != 0){

				return {emailTaken: true};

			}else{

				let newUser = new User({

					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
					email:reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10),
					mobileNo: reqBody.mobileNo

				})

					return newUser.save().then((result, error)=>{

					if (error){

						return {registration: false};

					}else{

						return {registration: true};

						//return "Successfully registered user";
					}
				})

				
			}
		})

}



module.exports.login = (reqBody)=>{

	return User.findOne({email: reqBody.email}).then((result, error)=>{

		if(result == null || result == undefined){

			return false;

		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect === true){


				 let tokenData = {

				 access: auth.createAccessToken(result.toObject()),
				 userId: result._id,
				 isAdmin: result.isAdmin,
				 firstName: result.firstName,
				 lastName: result.lastName

				}

				 ;

				 return tokenData;


			}else{

				return {password: false};;
			}

		}

	})

}

module.exports.userDetails = (userId)=>{

	return User.findOne({_id: userId}).then((result)=>{

		return result;

	})
	.catch(error=>{

		return error
	})


}

module.exports.setAsAdmin = (userId, isAdmin, userIsAdmin)=>{

	if(isAdmin === true){

		let setTo;
		let message;


		if (userIsAdmin === true) {

			setTo = false;
			message = "non-admin";

		}else{

			setTo = true;
			message = "admin";
		}

		return User.findByIdAndUpdate(userId, {isAdmin: setTo}).then((result, error)=>{

			return error ? false : {newStatus: message};

		})

	}else{

		return Promise.resolve("Not authorized to access this page");
	}

}

module.exports.showAllUsers = (isAdmin)=>{


	if(isAdmin === true){

		return User.find().then((result, error)=>{

			return error ? false : result;

		})

	}else{

		return Promise.resolve("Not authorized to access this page");
	}

}


module.exports.addToCart = async (userId, isAdmin, pId, qtyValue) => {


    if (isAdmin === true) {
        
        return;
    }

    try {
        const user = await User.findById(userId);

    if (user.cartItems.some(item=>item._id == pId)){

    	

    	return User.updateOne({_id: userId,"cartItems._id":pId}, {$inc:{"cartItems.$.quantity": qtyValue}})
    	.then(result=>{

    		return result;

    	})
    	.catch(error=>{

    		return error;
    	})

    }else{


    	user.cartItems.push({_id:pId, quantity: qtyValue});

    	const saveResult = await user.save();

    	return saveResult;

    }


    } catch (e) {

        console.log(e);
    }
};


/*module.exports.getCartItems = (userId, isAdmin)=>{

	if(isAdmin === true){

		return {error: "page not found"};
	}


	return User.findById(userId)

		.populate('cartItems._id')
		.then(result=>{

			return result.cartItems;

		}).catch(error=>{


			return error;
		})

}
*/

module.exports.getCartItems = (userId, isAdmin)=>{

	if(isAdmin === true){

		return {error: "page not found"};
	}

	return User.findById(userId)

		.populate('cartItems._id')
		.then(result=>{


			console.log(result.cartItems);


			let k=[];

			result.cartItems.forEach(e=>{

				if(e._id !== null && e._id.isActive === true){

						k.push({"_id":e._id._id, "quantity":e.quantity});

				}

			})

			return User.updateOne({"_id": userId}, {$set:{"cartItems":k}})
			.then(result=>{

					return User.findById(userId)

						.populate('cartItems._id')
						.then(res=>{

							return res.cartItems;

						}).catch(error=>{

							return error;
						})

			}

			)
			.catch(err => err);


		}).catch(error=>{


			return error;
		})

}




module.exports.removeFromCart = (userId,pId)=>{


 User.updateOne({_id: userId},{$pull:{cartItems:{_id:pId}}}).then((result)=>{

 	return result;

 }).catch(error=>{

 	return error;
 })


											
	return User.updateOne({_id: userId}, {$pull: {cartItems:pId}}).then((result)=>{

		return {removedFromCart: true};

	}).catch(error=>{

		//THIS THROWS THE ERROR I"M GETTING
		return error;
	
	})

}

module.exports.clearCart = (userId)=>{

	return User.updateOne({_id:userId},{$set:{cartItems:[]}}, {multi:true}).then((result)=>{

		if(result){

			return {cartCleared: true}
		}

	}).catch(error =>{

		console.log('MAY ERROR');

		return error;
	})

}

module.exports.changeQtyCart = (userId, operation, pId)=>{


	if(operation === "inc"){

		return User.updateOne({_id: userId,"cartItems._id":pId}, {$inc:{"cartItems.$.quantity": 1}})
		.then(result=>{


			return result;
		})

		.catch(error=>{


			return error;

		})

	}

	if(operation === "dec"){

		return User.updateOne({_id: userId,"cartItems._id":pId}, {$inc:{"cartItems.$.quantity": -1}})
		.then(result=>{


			return result;
		})

		.catch(error=>{


			return error;
			
		})

	}
	



}

