const Order = require('./../models/Order');
const Product = require('./../models/Product');
const User = require('./../models/User');
const auth = require('./../auth');
const collect = require('collect.js'); 
const mongoose = require('mongoose');



module.exports.checkoutOrder = (reqBody, buyerId, isAdmin)=>{


	console.log(reqBody.items);
	console.log(reqBody.totalAmt);

	if(isAdmin === true){

			return Promise.resolve('This is not an admin function');

		}else{


			let newOrder = new Order({

				buyerId: buyerId,
				totalAmount: reqBody.totalAmt,
				items: reqBody.items
			
			})

			return newOrder.save().then((result, error)=>{


			 	if(result){

			 		console.log(`WALA ERROR`, result)
			 		return result;

			 	}else{

			 		console.log(`MAY ERROR`, error)
			 		return error;
			 	}


			 })



	}





/*	if(isAdmin === true){

		return Promise.resolve('This is not an admin function');

	}else{


		return Product.find({'_id': {$in:reqBody.items}},'price')

			.then((result)=>{


				let sum = result.reduce((a, b)=>{

					return a.price +b.price;
				})


				let newOrder = new Order({

					buyerId: buyerId,
					totalAmount: sum,
					items: reqBody.items
				
				})


				return newOrder.save().then((result, error)=>{


				 	if(result){


				 		return result;

				 	}else{


				 		return error;
				 	}


				 })

			})

	}*/

}


module.exports.showOrders = (isAdmin)=>{

	if(isAdmin === true){


		return Order.find()
		.populate('buyerId')
		.populate('items.productId')
		.then(result=>{

			return result;

		}).catch(error=>{


			return error;
		})



/*		return Order.aggregate([

		    {$lookup:{
		        from:'users', // users collection name
		        localField:'buyerId',
		        foreignField:'_id',
		        as:'userArr'
		    }},
		    {
		        $lookup:{
		            from:'products', //items collection name
		            localField:'items.productId',
		            foreignField:'_id',
		            as:'productArr'
		        }
		    },

		]).then(result=>{

			return result;

		}). catch(error =>{

			console.log(error);
		})
*/

	}else{


		return Promise.resolve('Not authorized to access this page');
	}
}

module.exports.showMyOrders = (userId, isAdmin)=>{

	if (isAdmin === true) {

		return Promise.resolve('This is not an admin function');

	}else{


				return Order.find({buyerId: userId})
				.populate('items.productId')
				.then(result=>{

					return result;

				}).catch(error=>{


					return error;
				})
	


	}
}